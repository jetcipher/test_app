﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

namespace TestApp
{
    [Activity(Theme = "@style/FbnTheme", Label = "FBNApp",Icon = "@drawable/icon")]
    public class StatementActivity : AppCompatActivity
    {
        Android.Support.V4.Widget.DrawerLayout drawer;
        Android.Support.Design.Widget.NavigationView nav;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.statment);
            nav = FindViewById<Android.Support.Design.Widget.NavigationView>(Resource.Id.nav1);
            drawer = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer1);
            nav.NavigationItemSelected += Nav_NavigationItemSelected;
            // Create your application here
        }

        private void Nav_NavigationItemSelected(object sender, Android.Support.Design.Widget.NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.acc_nav:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.stat_nav:
                    StartActivity(typeof(StatementActivity));
                    break;
                default: return;
            }
        }
    }
}