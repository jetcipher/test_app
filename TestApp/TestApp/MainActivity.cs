﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Graphics;

namespace TestApp
{
    [Activity(Theme = "@style/FbnTheme", Label = "FBNApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity
    {
        TextView name, accTitle, accPrice, saveTitle, savePrice;
        Android.Support.V4.Widget.DrawerLayout drawer;
        Android.Support.Design.Widget.NavigationView nav;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            name = FindViewById<TextView>(Resource.Id.textName);
            name.SetColorTint(Resource.Color.icon_tint);
            accTitle = FindViewById<TextView>(Resource.Id.textAccTitle);
            accTitle.SetColorTint(Resource.Color.icon_tint);
            accPrice = FindViewById<TextView>(Resource.Id.textAccAmt);
            accPrice.SetColorTint(Resource.Color.icon_tint_light);
            saveTitle = FindViewById<TextView>(Resource.Id.textSavTitle);
            saveTitle.SetColorTint(Resource.Color.icon_tint);
            savePrice = FindViewById<TextView>(Resource.Id.textSavAmt);
            savePrice.SetColorTint(Resource.Color.icon_tint_light);
            nav = FindViewById<Android.Support.Design.Widget.NavigationView>(Resource.Id.nav1);
            drawer = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer1);
            nav.NavigationItemSelected += Nav_NavigationItemSelected;
            // Create your application here
        }

        private void Nav_NavigationItemSelected(object sender, Android.Support.Design.Widget.NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.acc_nav:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.stat_nav:
                    StartActivity(typeof(StatementActivity));
                    break;
                default: return;
            }
        }
    }

    public static class Extensions
    {
        public static void SetColorTint(this TextView tv, int color)
        {
            foreach (var item in tv.GetCompoundDrawables())
            {
                item?.SetColorFilter(new PorterDuffColorFilter(tv.Resources.GetColor(color), PorterDuff.Mode.SrcIn));
            }
        }
    }
}

